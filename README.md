![Nombre_proyecto](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2021/Sep/21/2037733242-8-documentacion-proyectos-logo_avatar.png)

## Nombre del proyecto - ejemplo Proyecto de Test

Agregar descripcion del proyecto.

### Agregar area de proyecto 


### Documentación API y ejemplos

## Tabla de Contenido 📋
1. [Comenzando 🚀](#markdown-header-comenzando)
2. [Instalación 🔧](#markdown-header-instalacion)
3. [Puesta en marcha 🔩](#markdown-header-puesta-en-marcha-a-namepuesta-en-marchaa)
4. [Documentación 📖](#markdown-header-documentacion)
5. [Ramas 🧬](#markdown-header-ramas)
6. [Otros 📚](#markdown-header-otros)
7. [Herramientas 🛠️](#markdown-header-herramientas)
7. [Licencia 📄](#markdown-header-licencia)

## Comenzando 🚀
- Agregar instrucciones del proyecto

## Instalación 🔧
- Agregar los pasos a seguir para la instalación del proyecto

## Puesta en marcha 🔩
- Agregar los pasos a seguir para colocar en marcha el proyecto en local, opcional agregar los pasos para producción

## Documentación 📖
- Agregar lo referente a documentacion si aplica

## Ramas 🧬
- Agregar las ramas principales creadas en el proyecto

## Otros 📚
- Agregar otro datos que se considere necesario en el proyecto

## Herramientas 🛠️
- Agregar herramientas, frameworks usado en el proyecto

## Licencia 📄

Este proyecto está bajo licencia privada, cualquier uso sin su consentimiento queda prohibido.


![Nombre de proyecto](https://img.shields.io/badge/Appyweb-Nombredeproyecto-blue)

> **Nota**: El archivo README.md está escrito en 'Markdown', mas información en [Markdown](https://markdown.es/sintaxis-markdown)


